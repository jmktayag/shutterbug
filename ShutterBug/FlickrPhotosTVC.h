//
//  FlickrPhotosTVC.h
//  ShutterBug
//
//  Created by Mckein on 12/17/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotosTVC : UITableViewController
//Public Api
@property (strong, nonatomic) NSArray *photos; // of Flickr photo NSDictionary

@end
